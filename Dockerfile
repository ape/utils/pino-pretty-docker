FROM node:14-alpine

ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin

RUN npm install -g pino-pretty

ENTRYPOINT ["pino-pretty", "-c", "-t", "-l", "-i", "hostname,pid"]

USER node

