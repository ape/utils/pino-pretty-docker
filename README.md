# Pino Pretty log formatter (node image with pino-pretty preinstalled)

## Usage

```shell
# Outputs Pino Pretty’s version (e.g. “4.3.0”)
docker run --rm --init pino-pretty --version

# Piping any stdout into Pino Pretty’s stdin
cat errors.log | docker run --rm --init -i pino-pretty

# Using extra arguments (i.e. filtering log entries matching a JMESPath)
cat errors.log | docker run --rm --init -i pino-pretty -s 'reqId'

```

### Default arguments

Docker entrypoint is set with the following arguments: 

- `-c` colorize 
- `-l` level first
- `-t` human readable date & time
- `-i hostname,pid` ignore hostname & pid
